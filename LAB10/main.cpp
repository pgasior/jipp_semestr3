#include <iostream>
#include "Fraction.h"
#include "Array.h"
#include "MyList.h"

int main()
{
    Array<Fraction> arr;
    MyList<Fraction> lst;
    Array<bool> bitfield;
    for(int i=0; i<10; ++i)
    {
        arr.push_back(i);
        lst.push_back(i);
        bitfield.push_back(i%2);
    }
    for(int i=0; i<arr.size(); ++i)
        std::cout << "Array: " << i << ": " << arr[i] << "\n";
    for(MyList<Fraction>::Iterator it=lst.begin(); it!=lst.end(); it++)
        std::cout << "List: " << ": " << *it << "\n";

    for(int i=0; i<bitfield.getSize(); ++i)
        std::cout << "Bitfield: " << i << ": " << bitfield[i] <<"\n";
    std::cout << "Elementow: " << bitfield.getSize() << "; ";
    std::cout << "Pamiec: " << bitfield.getCapacity() << "\n";
}
