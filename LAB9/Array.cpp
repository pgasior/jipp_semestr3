#include "Array.h"
#include "Fraction.h"
#include <cstdlib>
class unknown_element_exception : public std::exception
{

  virtual const char* what() const throw()
  {
    return "Probowano uzyskac dostep do nieistniejacego elementu ";
  }
} ;
Array::Array()
{
    _size=0;
    try
    {
        tab = new Fraction[_size];
    }

    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge utowrzyc tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
}
Array::Array(int size)
{
    _size=size;
    if(size>20)
        throw (int)-1;
    try
    {
        tab = new Fraction[_size];
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge utowrzyc tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
}

Array::~Array()
{
    if(tab!=NULL)
        delete[](tab);
}

void Array::push_back(Fraction newfrac)
{
    if((_size+1)>20)
        throw too_big_exception();
    _size++;
    Fraction *tab_new;
    try
    {
        tab_new = new Fraction[_size];
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge dodac elementu do tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
    for(int i=0;i<_size-1;i++)
        tab_new[i]=tab[i];
    delete[](tab);
    tab=tab_new;
    tab[_size-1]=newfrac;
}

int Array::size()
{
    return _size;
}
void Array::clear()
{
    delete(tab);
    tab=NULL;
}
Fraction& Array::operator[] (int i)
{
    if(i<0 || i>=_size)
        throw unknown_element_exception();
    return tab[i];
}

Array::Iterator Array::begin()
{
    return Array::Iterator(tab);
}
Array::Iterator Array::end()
{
    return Array::Iterator(tab+_size);
}
