#ifndef CONTAINER_H
#define CONTAINER_H
#include "Fraction.h"
//#include "IteratorInterface.h"
class too_big_exception : public std::exception
{
  virtual const char* what() const throw()
  {
    return "Probowano dodac wiecej niz 20 elementow";
  }
} ;
class Container
{
public:
        virtual void push_back(Fraction) = 0;
        //virtual Iterator begin() = 0;
        //virtual Iterator end() = 0;
        virtual int size() = 0;
    protected:
        int _size;
    private:
};

#endif // CONTAINER_H
