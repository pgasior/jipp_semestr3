#ifndef FRACTION_H
#define FRACTION_H
#include <iostream>

class Fraction
{
public:
    Fraction(int fracnom=0, int fracdenom=1);

    Fraction(double number);
    Fraction(const char *strnum);
    int nominator() const;
    int denominator() const;
    void add(const Fraction &ulamekb);

    operator float();
    operator double();

    Fraction operator+(const Fraction &ulamekb);
    Fraction operator-(const Fraction &ulamekb);
    Fraction operator*(const Fraction &ulamekb);
    Fraction operator/(const Fraction &ulamekb);
    Fraction operator-() const;
    Fraction operator!() const;
    Fraction operator++(int); //postfix
    Fraction& operator++();                  //prefix
    friend std::ostream &operator<<( std::ostream &output, const Fraction &ulamek );
    int operator[](int i);
    int operator()(int i);



protected:
    int fracnom;
    int fracdenom;
    int NWD(int a, int b);
    int NWW(int a, int b);
    void reduce();
    //const int _dokladnosc = 5;
};

#endif // FRACTION_H
