#include "wektor4D.h"
#include <exception>
#include "Fraction.h"
#include <cmath>

wektor4D::wektor4D(Fraction x,Fraction y,Fraction z,Fraction w)
try
{
    {
        _x=x;
        _y=y;
        _z=z;
        _w=w;
    }
}
catch(std::exception& e)
{
    std::cout << e.what() << std::endl;
}

Fraction wektor4D::getX() const
{
    return _x;
}
Fraction wektor4D::getY() const
{
    return _y;
}
Fraction wektor4D::getZ() const
{
    return _z;
}
Fraction wektor4D::getW() const
{
    return _w;
}

wektor4D wektor4D::operator+(const wektor4D &arg2)
{
    return wektor4D(_x+arg2.getX(),_y+arg2.getY(),_z+arg2.getZ(),_w+arg2.getW());
}
wektor4D wektor4D::operator-(const wektor4D &arg2)
{
    return wektor4D(_x-arg2.getX(),_y-arg2.getY(),_z-arg2.getZ(),_w-arg2.getW());
}
wektor4D wektor4D::operator*(double liczba)
{
    return wektor4D(_x*Fraction(liczba),_y*Fraction(liczba),_z*Fraction(liczba),_w*Fraction(liczba));
}

double wektor4D::operator*(const wektor4D &arg2)
{
    return _x*arg2.getX()+_y*arg2.getY()+_z*arg2.getZ()+_w*arg2.getW();
}
wektor4D wektor4D::operator/(double liczba)
{
    return wektor4D(_x/Fraction(liczba),_y/Fraction(liczba),_z/Fraction(liczba),_w/Fraction(liczba));
}
double wektor4D::operator/(const wektor4D &arg2)
{
    return _x/arg2.getX()+_y/arg2.getY()+_z/arg2.getZ()+_w/arg2.getW();
}
double wektor4D::len()
{
    return std::sqrt(double(_x*_x+_y*_y+_z*_z+_w*_w));
}
wektor4D wektor4D::normalzie()
{
    double length = len();
    if (length ==0)
        throw zero_length_exception();
    return *this/length;
}
