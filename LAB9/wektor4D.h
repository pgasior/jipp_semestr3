#ifndef WEKTOR4D_H
#define WEKTOR4D_H
#include "Fraction.h"

class zero_length_exception : public std::exception
{
  virtual const char* what() const throw()
  {
    return "Dlugosc wynosi 0";
  }
};

class wektor4D
{
    public:
        wektor4D(Fraction x,Fraction y,Fraction z,Fraction w);
        wektor4D() {}
        wektor4D operator+(const wektor4D &arg2);
        wektor4D operator-(const wektor4D &arg2);
        double operator*(const wektor4D &arg2);
        wektor4D operator*(double liczba);
        wektor4D operator/(double liczba);
        double operator/(const wektor4D &arg2);
        double len();
        Fraction getX() const;
        Fraction getY() const;
        Fraction getZ() const;
        Fraction getW() const;
        wektor4D normalzie();

    protected:
    private:
        Fraction _x;
        Fraction _y;
        Fraction _z;
        Fraction _w;
};

#endif // WEKTOR4D_H
