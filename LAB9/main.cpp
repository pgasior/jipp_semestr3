#include <iostream>
#include "Fraction.h"
#include "MyList.h"
#include "Array.h"
#include "wektor4D.h"
#include <exception>

using namespace std;

int main()
{
    MyList lista1;
    Array arr1;
    //lista.push_back(5);
    //arr.push_back(5);
    cout << "Proba dodania ulamka z mianownikiem 0 do tablicy" << endl;
    try
    {
        arr1.push_back(Fraction(5,1));
        arr1.push_back(Fraction(5,0));
        arr1.push_back(Fraction(5,2));
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
    cout << endl << "Proba dodania ulamka z mianownikiem 0 do listy" << endl;
    try
    {
        lista1.push_back(Fraction(5,1));
        lista1.push_back(Fraction(5,0));
        lista1.push_back(Fraction(5,2));
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
    Array arr2;
    MyList lista2;
    cout << endl <<"Proba wypelnienia tablicy 25 elementami" << endl;
    try
    {
        for(int i=0; i<25; i++)
            arr2.push_back(i);
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }
    cout << endl <<"Proba wypelnienia listy 25 elementami" << endl;
    try
    {
        for(int i=0; i<25; i++)
            lista2.push_back(i);
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }

    cout << endl <<"Proba wypelnienia odczytania z tablicy elementow od -2 do size()+2" << endl;
    for(int i=-2; i<=arr2.size()+2; i++)
    {
        try
        {
            cout << "element " << i << ": " << arr2[i] << endl;
        }
        catch(exception& e)
        {
            cout << e.what() << endl;
        }
    }

    cout << endl << "Proba odczytania elementu przed pierwszym elementem listy" << endl;
    try
    {
        MyList::Iterator arrit = lista2.begin();
        arrit--;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }

    cout << endl << "Proba odczytania elementu za ostatnim elementem listy" << endl;
    try
    {
        MyList::Iterator arrit = lista2.end();
        arrit++;
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }


    wektor4D wek;
    try
    {
        wek = wektor4D(Fraction(0,1),Fraction(0,1),Fraction(0,1),Fraction(0,1));
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }

    wektor4D wek2;
    try
    {
        wek2 = wek.normalzie();
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }

    cout << "X: " << wek2.getX() <<endl;
    cout << "Y: " << wek2.getY() <<endl;
    cout << "Z: " << wek2.getZ() <<endl;
    cout << "W: " << wek2.getW() <<endl;





    return 0;
}
