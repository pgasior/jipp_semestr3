#ifndef FRACTIONAVGMINMAX_H
#define FRACTIONAVGMINMAX_H
#include "IteratorMinMax.h"
#include "IteratorSrednia.h"
#include "Fraction.h"


class FractionAvgMinMax : public Fraction,protected IteratorMinMax,protected IteratorSrednia
{
    public:
        FractionAvgMinMax();
        FractionAvgMinMax(Fraction * frac);
        void operator=(Fraction *frac);
        FractionAvgMinMax operator++(int); //postfix
        FractionAvgMinMax& operator++();     //prefix
        FractionAvgMinMax operator--(int); //postfix
        FractionAvgMinMax& operator--();     //prefix
        void next();
        bool operator<(const Fraction* arg2);
        bool operator>(const Fraction* arg2);
        bool operator==(const Fraction* arg2);
        Fraction* getMin();
        Fraction* getMax();
    protected:
        void update();
    private:
};

#endif // FRACTIONAVGMINMAX_H
