#include "FractionAvgMinMax.h"
#include "IteratorMinMax.h"
#include "IteratorSrednia.h"
#include "Iterator.h"
#include "Fraction.h"
FractionAvgMinMax::FractionAvgMinMax()
{
    pusty = true;
}
FractionAvgMinMax::FractionAvgMinMax(Fraction * frac) :  IteratorMinMax(frac), IteratorSrednia(frac)//,Iterator(frac)
{
//    fracptr=frac;
//    IteratorMinMax::update();
//    IteratorSrednia::update();
    pusty=false;
    update();
}
void FractionAvgMinMax::operator=(Fraction *frac)
{
    fracptr=frac;
    IteratorMinMax::update();
    IteratorSrednia::update();
    update();

}
void FractionAvgMinMax::update()
{
    Fraction tmp(suma/ile_elementow);
    fracnom=tmp.nominator();
    fracdenom=tmp.denominator();
}

FractionAvgMinMax FractionAvgMinMax::operator++(int)
{
    FractionAvgMinMax tmp = fracptr;
    fracptr++;
    IteratorMinMax::update();
    IteratorSrednia::update();
    update();
    return tmp;
}
FractionAvgMinMax& FractionAvgMinMax::operator++()     //prefix
{
    fracptr++;
    IteratorMinMax::update();
    IteratorSrednia::update();
    update();
    return *this;
}
FractionAvgMinMax FractionAvgMinMax::operator--(int) //postfix
{
    FractionAvgMinMax tmp = fracptr;
    fracptr--;
    IteratorMinMax::update();
    IteratorSrednia::update();
    update();
    return tmp;
}
FractionAvgMinMax& FractionAvgMinMax::operator--()     //prefix
{
    fracptr--;
    IteratorMinMax::update();
    IteratorSrednia::update();
    update();
    return *this;
}

void FractionAvgMinMax::next()
{
    fracptr++;
    IteratorMinMax::update();
    IteratorSrednia::update();
    update();
}

bool FractionAvgMinMax::operator<(const Fraction* arg2)
{
    return fracptr<arg2;
}
bool FractionAvgMinMax::operator>(const Fraction* arg2)
{
    return fracptr>arg2;
}
bool FractionAvgMinMax::operator==(const Fraction* arg2)
{
    return fracptr==arg2;
}
Fraction* FractionAvgMinMax::getMax()
{
    return _maxPtr;
}

Fraction* FractionAvgMinMax::getMin()
{
    return _minPtr;
}
