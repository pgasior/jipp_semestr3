#include "IteratorMinMax.h"
#include "Iterator.h"
#include "Fraction.h"
#include <iostream>

using std::cout;
using std::endl;


IteratorMinMax::IteratorMinMax() //: Iterator()
{
    pusty = true;
}


IteratorMinMax::IteratorMinMax(Fraction *frac) : Iterator(frac)
{
    pusty=false;
    _min=double(*fracptr);
    _minPtr = fracptr;
    _max=double(*fracptr);
    _maxPtr = fracptr;
    update();
}

void IteratorMinMax::update()
{
    if (pusty)
    {
        _min=double(*fracptr);
        _minPtr = fracptr;
        _max=double(*fracptr);
        _maxPtr = fracptr;
        pusty=false;
    }
    else
    {
        if(double(*fracptr)<_min)
        {
            _min=double(*fracptr);
            _minPtr=fracptr;
        }

        if(double(*fracptr)>_max)
        {
            _max=double(*fracptr);
            _maxPtr=fracptr;
        }
    }

}

void IteratorMinMax::operator=(Fraction *frac)
{
    fracptr=frac;
    update();
}

IteratorMinMax IteratorMinMax::operator++(int)
{
    IteratorMinMax tmp = fracptr;
    fracptr++;
    update();
    return tmp;
}
IteratorMinMax& IteratorMinMax::operator++()     //prefix
{
    fracptr++;
    update();
    return *this;
}
IteratorMinMax IteratorMinMax::operator--(int) //postfix
{
    IteratorMinMax tmp = fracptr;
    fracptr--;
    update();
    return tmp;
}
IteratorMinMax& IteratorMinMax::operator--()     //prefix
{
    fracptr--;
    update();
    return *this;
}
Fraction* IteratorMinMax::getMax()
{
    return _maxPtr;
}

Fraction* IteratorMinMax::getMin()
{
    return _minPtr;
}

void IteratorMinMax::next()
{
    fracptr++;
    update();
}
