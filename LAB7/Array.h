#ifndef ARRAY_H
#define ARRAY_H
#include "Fraction.h"

class Array
{
    public:
        Array();
        ~Array();
        void push_back(Fraction newfrac);
        Fraction* begin();
        Fraction* end();
        int size();
        void clear();
        Fraction& operator[](int i);

    protected:
    private:
        Fraction* tab;
        int _size;
};

#endif // ARRAY_H
