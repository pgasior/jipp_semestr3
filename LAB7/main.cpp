#include <iostream>
#include "Fraction.h"
#include "Iterator.h"
#include "IteratorSrednia.h"
#include "IteratorMinMax.h"
#include "FractionAvgMinMax.h"
#include "Array.h"
#define M_PI 3.14159265358979323846
using namespace std;

int main()
{
    Array arr;
// Dodawanie element�w
    arr.push_back(Fraction(1));
    arr.push_back(2);
    arr.push_back(Fraction(3));

    cout << "Iterator: " <<endl;
    for(Iterator it=arr.begin(); it<arr.end(); it.next())
        std::cout << *it << "\n";


    cout << endl << "IteratorSrednia: " <<endl;
    IteratorSrednia it_sr;
    for(it_sr=arr.begin(); it_sr<arr.end()-1;it_sr.next())
    {
    }
    cout <<"Srednia: " << it_sr.srednia() << endl;

    cout << endl << "IteratorMinMax: " <<endl;
    IteratorMinMax it_minmax;
    for(it_minmax=arr.begin(); it_minmax<arr.end()-1;it_minmax.next())
    {
    }
    cout << "Max: " << *it_minmax.getMax() << endl;
    cout << "Min: " << *it_minmax.getMin() << endl;

    cout << endl << "FractionAvgMinMax: " <<endl;
    FractionAvgMinMax famm;
    for(famm=arr.begin(); famm<arr.end()-1; famm.next())
    {
    }
    cout << "Srednia: " << famm << endl;
    cout << "Max: " << *famm.getMax() << endl;
    cout << "Min: " << *famm.getMin() << endl;

    cout << endl << "operator[]" << endl;
    for(int i=0; i<arr.size(); ++i)
        std::cout << arr[i] << "\n";

    arr.clear();
    return 0;
}
