#ifndef ITERATORSREDNIA_H
#define ITERATORSREDNIA_H

#include "Iterator.h"
#include "Fraction.h"


class IteratorSrednia : virtual public Iterator
{
    public:
        IteratorSrednia(Fraction* frac);
        IteratorSrednia();
        void operator=(Fraction *frac);
        IteratorSrednia operator++(int); //postfix
        IteratorSrednia& operator++();     //prefix
        IteratorSrednia operator--(int); //postfix
        IteratorSrednia& operator--();     //prefix
        double srednia();
        void next();
    protected:
        void update();

        double suma;
        int ile_elementow;
    private:
};

#endif // ITERATORSREDNIA_H
