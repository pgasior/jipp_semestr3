#include "IteratorSrednia.h"
#include "Iterator.h"
#include "Fraction.h"
#include <iostream>

IteratorSrednia::IteratorSrednia() : Iterator()
{
    suma= 0;
    ile_elementow = 0;
}

IteratorSrednia::IteratorSrednia(Fraction *frac) : Iterator(frac)
{
    suma = 0;
    ile_elementow = 0;
    update();
}
double IteratorSrednia::srednia()
{
    return suma/ile_elementow;
}
void IteratorSrednia::update()
{
    suma+=double(*fracptr);
    ile_elementow++;
}

void IteratorSrednia::operator=(Fraction *frac)
{
    fracptr=frac;
    update();
}

IteratorSrednia IteratorSrednia::operator++(int)
{
    IteratorSrednia tmp = fracptr;
    fracptr++;
    update();
    return tmp;
}
IteratorSrednia& IteratorSrednia::operator++()     //prefix
{
    fracptr++;
    update();
    return *this;
}
IteratorSrednia IteratorSrednia::operator--(int) //postfix
{
    IteratorSrednia tmp = fracptr;
    fracptr--;
    update();
    return tmp;
}
IteratorSrednia& IteratorSrednia::operator--()     //prefix
{
    fracptr--;
    update();
    return *this;
}

void IteratorSrednia::next()
{
    fracptr++;
    update();
}

