#ifndef ITERATORMINMAX_H
#define ITERATORMINMAX_H

#include "Iterator.h"
#include "Fraction.h"


class IteratorMinMax : virtual public Iterator
{
    public:
        IteratorMinMax();
        IteratorMinMax(Fraction* frac);
        void operator=(Fraction *frac);
        IteratorMinMax operator++(int); //postfix
        IteratorMinMax& operator++();     //prefix
        IteratorMinMax operator--(int); //postfix
        IteratorMinMax& operator--();     //prefix
        Fraction* getMin();
        Fraction* getMax();
        void next();
    protected:
        void update();

        double _min;
        Fraction *_minPtr;
        double _max;
        Fraction *_maxPtr;
        bool pusty;

};

#endif // ITERATORMINMAX_H
