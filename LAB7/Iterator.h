#ifndef ITERATOR_H_INCLUDED
#define ITERATOR_H_INCLUDED
#include "Fraction.h"
class Iterator
{
public:
    Iterator();
    Iterator(Fraction *frac);
    Fraction& operator*();
    void operator=(Fraction *frac);
    Fraction* operator->();
    Iterator operator++(int); //postfix
    Iterator& operator++();     //prefix
    Iterator operator--(int); //postfix
    Iterator& operator--();     //prefix
    bool operator<(const Fraction* arg2);
    bool operator>(const Fraction* arg2);
    bool operator==(const Fraction* arg2);
    void next();


protected:
    Fraction *fracptr;
};

#endif // ITERATOR_H_INCLUDED
