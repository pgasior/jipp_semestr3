#include "Fraction.h"
#include <iostream>
#include <cstdlib>
#include <cmath>

Fraction::Fraction(int fracnom, int fracdenom) : fracnom(fracnom),fracdenom(fracdenom)
{
    this->reduce();
}

Fraction::Fraction(const char *strnum)
{
    Fraction tmp(atof(strnum));
    fracnom=tmp.nominator();
    fracdenom=tmp.denominator();
}

Fraction::Fraction(double number)
{
    fracnom=static_cast<int>(number);
    fracdenom=1;
    double tmp = number-fracnom;
    if(tmp!=0)
    {
        int cyfrymax=5;                 //dokladnosc
        for(int i=1; i<=cyfrymax; i++)
            tmp*=10;

        int tmp2;
        tmp2=static_cast<int>(tmp);
        while(tmp2%10==0)
        {
            tmp2/=10;
            cyfrymax--;
        }
        for(int i=1; i<=cyfrymax; i++)
        {
            fracnom*=10;
            fracdenom*=10;
        }
        fracnom+=tmp2;
    }
    this->reduce();
}

int Fraction::nominator() const
{
    return fracnom;
}

int Fraction::denominator() const
{
    return fracdenom;
}

void Fraction::add(const Fraction &ulamekb)
{
    if(fracdenom==ulamekb.denominator())
    {
        fracnom+=ulamekb.nominator();
    }
    else
    {
        int nww=NWW(fracdenom,ulamekb.denominator());
        fracnom=fracnom*(nww/fracdenom)+ulamekb.nominator()*(nww/ulamekb.denominator());
        fracdenom=nww;
    }
    this->reduce();
}

Fraction::operator float()
{
    return float(fracnom)/float(fracdenom);
}

Fraction::operator double()
{
    return double(fracnom)/double(fracdenom);
}

int Fraction::NWD(int a, int b)
{
    if(fracnom==0)
        return std::abs(fracdenom);
    while(a!=b)
        if(a>b)
            a-=b;
        else
            b-=a;
    return a;
}

int Fraction::NWW(int a, int b)
{
    return a*b/NWD(a,b);
}

void Fraction::reduce()
{
    int nomsign = 1;
    int denomsign = 1;

    if(fracnom<0&&fracdenom<0)
    {
        fracnom=-fracnom;
        fracdenom=-fracdenom;
    }
    if(fracnom>0&&fracdenom<0)      //w przypadku ujemnego ulamka to licznik zawsze jest ujemny
    {
        fracnom=-fracnom;
        fracdenom=-fracdenom;
    }
    //NWD nie dziala na ujemnych, wiec zapamietuje znak i robie dodatnie
    if(fracnom<0)
    {
        nomsign=-1;
        fracnom*=-1;
    }

    if(fracdenom<0)
    {
        denomsign=-1;
        fracdenom*=-1;
    }
    int nwd = NWD(fracnom,fracdenom);
    fracnom=fracnom/nwd;
    fracdenom=fracdenom/nwd;
    //przywracam znak
    fracnom*=nomsign;
    fracdenom*=denomsign;
}

Fraction Fraction::operator+(const Fraction &ulamekb)
{
    Fraction tmp(fracnom,fracdenom);
    tmp.add(ulamekb);
    return tmp;
}
Fraction Fraction::operator-(const Fraction &ulamekb)
{
    Fraction tmp(fracnom,fracdenom);
    tmp.add(-ulamekb);
    return tmp;
}
Fraction Fraction::operator*(const Fraction &ulamekb)
{
    Fraction tmp(fracnom*ulamekb.nominator(),fracdenom*ulamekb.denominator());
    return tmp;
}
Fraction Fraction::operator/(const Fraction &ulamekb)
{
    Fraction tmp(fracnom,fracdenom);
    tmp=tmp*(!ulamekb);
    return tmp;
}
Fraction Fraction::operator-() const
{
    Fraction tmp(-fracnom,fracdenom);
    return tmp;
}
Fraction Fraction::operator!() const
{
    if(fracnom==0) return 0;
    Fraction tmp(fracdenom,fracnom);
    return tmp;
}
Fraction Fraction::operator++(int) //postfix
{
    Fraction tmp(fracnom,fracdenom);
    add(1);
    return tmp;
}
Fraction& Fraction::operator++()                  //prefix
{
    add(1);
    return *this;
}
std::ostream &operator<<( std::ostream &output, const Fraction &ulamek )
{
    output << "("<<ulamek.fracnom<<"/"<<ulamek.fracdenom<<")";
    output.flush();
    return output;
}
int Fraction::operator[](int i)
{
    if ((i>=5) || (i<0))
        return 0;
    double tmp = (double)fracnom/(double)fracdenom;
    double tmp2 = tmp-int(tmp);

    for(int j=0;j<=i;j++)
    {
        tmp2*=10;
    }
    int tmp_int = (int)tmp2;
    return tmp_int%10;
}
int Fraction::operator()(int i)
{
    if (i<0)
        return 0;
    int tmp = fracnom/fracdenom;
    for(int j=0;j<i;j++)
    {
        tmp/=10;
    }
    return tmp%10;
}
