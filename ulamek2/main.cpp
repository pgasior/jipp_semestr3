#include <cstdio>
#include <cmath>
#include <iostream>
#include "Fraction.h"
#define M_PI 3.14159265358979323846


double srednia(double a, double b ,double c)
{
    return (a+b+c)/3;
}
int main(int, const char **)
{
    Fraction u1(2,10);
    Fraction u2(-3,-2);
    std::cout << "u1="<<u1<<"\n";
    std::cout << "u2="<<u2<<"\n";
    std::cout << "u1+u2="<<u1+u2<<"\n";
    std::cout << "u1-u2="<<u1-u2<<"\n";
    std::cout << "u1*u2="<<u1*u2<<"\n";
    std::cout << "u1/u2="<<u1/u2<<"\n";
    std::cout << "!u1="<<!u1<<"\n";
    std::cout << "-u1="<<-u1<<"\n";
    std::cout << "u1++="<<u1++<<"\n";
    std::cout << "u1="<<u1<<"\n";
    std::cout << "++u1="<<++u1<<"\n";
    std::cout << "u1="<<u1<<"\n";

    return 0;
}
