#ifndef ITERATOR_H
#define ITERATOR_H

#include "Fraction.h"
#include <iostream>
using namespace std;
class Iterator
{
    public:
        virtual Iterator& operator++()=0;
        virtual Fraction* operator->()=0;
        virtual Fraction & operator*()=0;
        virtual Iterator& operator--() = 0;     //prefix
//        virtual bool operator<(const Fraction* arg2);
//        virtual bool operator>(const Fraction* arg2);
        virtual bool operator==(const Iterator& arg2)  = 0; //{cout << "Porownanie z Iterator\n";return  0;}
        virtual bool operator!=(const Iterator& arg2) = 0;
    virtual Fraction *get() const = 0;
    protected:
    private:
};

#endif // ITERATOR_H
