#ifndef ARRAY_H
#define ARRAY_H

#include "Container.h"
#include "Fraction.h"
//#include "IteratorInterface.h"
#include "Iterator.h"
#include <iostream>
using namespace std;

class Array : public Container
{

    public:
        class Iterator : public ::Iterator
        {
        public:
            Iterator(Fraction *fracptr) : fracptr(fracptr) {}
            Iterator& operator++()
            {
                fracptr++;
                return *this;
            }
            Iterator operator++(int)
            {
                Iterator tmp = fracptr;
                fracptr++;
                return tmp;
            }
            Iterator& operator--()
            {
                fracptr--;
                return *this;
            }
            Iterator operator--(int)
            {
                Iterator tmp = fracptr;
                fracptr--;
                return tmp;
            }
            bool operator==(const ::Iterator& arg2)
            {
                //cout << "Porownanie z Array\n";
                return fracptr==arg2.get();
            }
            bool operator!=(const ::Iterator& arg2)
            {
                return fracptr!=arg2.get();
            }
            Fraction* operator->()
            {
                return fracptr;
            }
            Fraction& operator*()
            {
                return *fracptr;
            }
            Fraction* get() const
            {
                return fracptr;
            }
            Iterator(const Iterator& it)
            {
                fracptr = it.get();
            }
        private:
            Fraction* fracptr;
        };
        Array();
        ~Array();
        void push_back(Fraction newfrac);
        Iterator begin();
        Iterator end();
        int size();
        void clear();
        Fraction& operator[](int i);

    protected:
    private:
        Fraction* tab;
        int _size;

};

#endif // ARRAY_H
