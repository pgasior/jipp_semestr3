#include "MyList.h"

MyList::MyList()
{
    list_end = new MyList::Node();
    //list_end->setVal(NULL);
    list_end->setNext(NULL);
    list_end->setNext(NULL);
    front = back = NULL;// fracptr =NULL;

    _size = 0;
}

MyList::~MyList()
{
    while(front->getNext()!=NULL)
    {
        MyList::Node* tmp = front->getNext();
        delete front->getVal();
        delete front;
        front = tmp;
    }
    delete list_end;
}


//void MyList::push_front(Fraction frac)
//{
//    MyList::Node *p = new MyList::Node();
//    p->setVal(frac);
//    p->setNext(front);
//    p->setPrev(NULL);
//    if(front) front->setPrev (p);
//    front = p;
//    if(!back) back = front;
//    _size++;
//}

void MyList::push_back(Fraction frac)
{
    MyList::Node *p = new MyList::Node();
    Fraction *tmpfrac = new Fraction(frac);
    p->setVal(tmpfrac);
    if(back) back->setNext(p);
    p->setNext(list_end);
    p->setPrev(back);
    back = p;
    list_end->setPrev(back);
    //fracptr = *(back->getVal());
    if(!front) front = back;
    _size++;
}
MyList::Iterator MyList::begin()
{
    return Iterator(front);
}
MyList::Iterator MyList::end()
{
    return Iterator(list_end);
}
int MyList::size()
{
    return _size;
}


MyList::Node::Node()
{
    prev = next = NULL;
    val = NULL;
}
MyList::Node* MyList::Node::getPrev()
{
    return prev;
}
MyList::Node* MyList::Node::getNext()
{
    return next;
}
Fraction* MyList::Node::getVal()
{
    return val;
}
void MyList::Node::setPrev(MyList::Node* newPrev)
{
    prev = newPrev;
}
void MyList::Node::setNext(MyList::Node* newNext)
{
    next = newNext;
}
void MyList::Node::setVal(Fraction* newVal)
{
    val = newVal;
}

//MyList::Iterator MyList::Iterator::operator++(int) //postfix
//{
//    MyList::Iterator tmp( fracptr);
//    fracptr++;
//    return tmp;
//}
//MyList::Iterator& MyList::Iterator::operator++()    //prefix
//{
//    fracptr++;
//    return *this;
//}
//MyList::Iterator MyList::Iterator::operator--(int) //postfix
//{
//    MyList::Iterator tmp(fracptr);
//    fracptr--;
//    return tmp;
//}
//MyList::Iterator& MyList::Iterator::operator--()     //prefix
//{
//    fracptr--;
//    return *this;
//}
