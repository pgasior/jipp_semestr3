#ifndef MYLIST_H
#define MYLIST_H

#include "Container.h"
#include "Fraction.h"
#include "Iterator.h"


class MyList : public Container
{


    public:
        class Node
        {
        public:
            Node();
            Node* getPrev();
            Node* getNext();
            Fraction* getVal();
            void setPrev(Node* newPrev);
            void setNext(Node* newNext);
            void setVal(Fraction* newVal);
        private:
            Node *next;
            Node *prev;
            Fraction *val;
        };
        class Iterator : public ::Iterator
        {
        public:
            Iterator(Node *fracnode) : fracnode(fracnode)
            {
                if(fracnode==NULL)
                    fracptr=NULL;
                else fracptr=fracnode->getVal();
            }
            Iterator& operator++()
            {
                fracnode=fracnode->getNext();
//                if (fracnode==NULL)
//                    fracptr=NULL;
                fracptr=(fracnode->getVal());
                return *this;
            }
            Iterator operator++(int)
            {
                Iterator tmp = fracnode;
                fracnode=fracnode->getNext();
//                if (fracnode==NULL)
//                    fracptr=NULL;
                fracptr=(fracnode->getVal());
                return tmp;
            }
            Iterator& operator--()
            {
                fracnode=fracnode->getPrev();
                fracptr=fracnode->getVal();
                return *this;
            }
            Iterator operator--(int)
            {
                Iterator tmp = fracnode;
                fracnode=fracnode->getPrev();
                fracptr=fracnode->getVal();
                return tmp;
            }
            bool operator==(const ::Iterator& arg2)
            {
                return fracptr==arg2.get();
            }
            bool operator!=(const ::Iterator& arg2)
            {
                return fracptr!=arg2.get();
            }
            Fraction* operator->()
            {
                return (fracnode->getVal());
            }
            Fraction& operator*()
            {
                return *fracnode->getVal();
            }
            Node* getnode() const
            {
                return fracnode;
            }
             Fraction* get() const
            {
                return fracptr;
            }
            Iterator(const Iterator& it)
            {
                fracnode = it.getnode();
            }


        private:
            Node* fracnode;
            Fraction *fracptr;
        };
        MyList();
        ~MyList();



        void push_back(Fraction);
        void push_front(Fraction frac);
        Iterator begin();
        Iterator end();
        int size();
    protected:
    private:
        Node *front;
        Node *back;
        Node *list_end;
        //Fraction *fracptr;
};

#endif // MYLIST_H
