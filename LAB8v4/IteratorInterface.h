#ifndef ITERATORINTERFACE_H
#define ITERATORINTERFACE_H
#include "Fraction.h"


class IteratorInterface
{
public:
//    virtual Fraction& operator*() = 0;

//    virtual void operator=(Fraction *frac) = 0;
//    virtual Fraction* operator->() = 0;
    virtual IteratorInterface& operator++(int) = 0; //postfix
//    virtual IteratorInterface& operator++() = 0;     //prefix
//    virtual IteratorInterface operator--(int) = 0; //postfix
//    virtual IteratorInterface& operator--() = 0;     //prefix
//    virtual bool operator<(const Fraction* arg2) = 0;
//    virtual bool operator>(const Fraction* arg2) = 0;
//    virtual bool operator==(const IteratorInterface& arg2) const = 0;
    virtual void next() = 0;
protected:
private:
};

#endif // ITERATORINTERFACE_H
