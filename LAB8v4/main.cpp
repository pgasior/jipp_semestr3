#include <iostream>
#include "Array.h"
#include "MyList.h"
#include "Fraction.h"
#include <cstdio>
#include <cstdlib>
#include <ctime>


using namespace std;

int main()
{
    srand(time(NULL));
    Array arr;
    MyList lst;
    for(int i=0; i<10; ++i)
    {
        arr.push_back(rand()%10/(i+1.0f));
        lst.push_back(rand()%10/(i+1.0f));
    }
    std::cout << "Array:\n";

    for(Array::Iterator it=arr.begin(); it!=arr.end(); ++it)
        std::cout << *it << "\n";
    std::cout << "\nList:\n";
    //cout << *lst.end() << endl;
    for(MyList::Iterator it=lst.begin(); it!=lst.end(); it++)
        std::cout << *it << "\n";

}
