#ifndef FRACTION_H
#define FRACTION_H


class Fraction
{
    public:
        Fraction(int fracnom, int fracdenom);
        Fraction(int fracnom);
        Fraction(double number);
        Fraction(const char *strnum);
        int nominator();
        int denominator();
        void add(Fraction ulamekb);

        operator float();
        operator double();



    private:
        int fracnom;
        int fracdenom;
        int NWD(int a, int b);
        int NWW(int a, int b);
        void reduce();
};

#endif // FRACTION_H
