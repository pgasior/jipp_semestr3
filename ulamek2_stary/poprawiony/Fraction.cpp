#include "Fraction.h"
#include <iostream>
#include <cstdlib>
using namespace std;
Fraction::Fraction(int fracnom, int fracdenom) : fracnom(fracnom),fracdenom(fracdenom)
{
    this->reduce();
}
Fraction::Fraction(int fracnom) : fracnom(fracnom),fracdenom(1) {}
Fraction::Fraction(const char *strnum)
{
    Fraction tmp(atof(strnum));
    fracnom=tmp.nominator();
    fracdenom=tmp.denominator();
}
Fraction::Fraction(double number)
{
    fracnom=static_cast<int>(number);
    fracdenom=1;
    double tmp = number-fracnom;
    if(tmp!=0)
    {
        int cyfrymax=5;                 //dokladnosc
        for(int i=1; i<=cyfrymax; i++)
            tmp*=10;

        int tmp2;
        tmp2=static_cast<int>(tmp);
        while(tmp2%10==0)
        {
            tmp2/=10;
            cyfrymax--;
        }
        for(int i=1; i<=cyfrymax; i++)
        {
            fracnom*=10;
            fracdenom*=10;
        }
        fracnom+=tmp2;
    }
    this->reduce();
}

int Fraction::nominator()
{
    return fracnom;
}

int Fraction::denominator()
{
    return fracdenom;
}

void Fraction::add(Fraction ulamekb)
{
    if(fracdenom==ulamekb.denominator())
    {
        fracnom+=ulamekb.nominator();
    }
    else
    {
        int nww=NWW(fracdenom,ulamekb.denominator());
        fracnom=fracnom*(nww/fracdenom)+ulamekb.nominator()*(nww/ulamekb.denominator());
        fracdenom=nww;
    }
    this->reduce();
}

Fraction::operator float()
{
    return float(fracnom)/float(fracdenom);
}

Fraction::operator double()
{
    return double(fracnom)/double(fracdenom);
}

int Fraction::NWD(int a, int b)
{
    while(a!=b)
        if(a>b)
            a-=b;
        else
            b-=a;
    return a;
}

int Fraction::NWW(int a, int b)
{
    return a*b/NWD(a,b);
}

void Fraction::reduce()
{
    int nwd = NWD(fracnom,fracdenom);
    fracnom=fracnom/nwd;
    fracdenom=fracdenom/nwd;
}


