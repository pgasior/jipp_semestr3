/**************************************************************************************************
 * JiPP, Laboratorium 04
 *
 * Zadanie 1 (3.5): Napisa� klas� reprezentuj�c� u�amek zwyk�y. Licznik i mianownik
 *  powinny by� typu ca�kowitego. Mo�liwe ma by� rzutowanie:
 *   - Jednej liczby ca�kowitej na u�amek zwyk�y (mianownik = 1),
 *   - Dw�ch liczb ca�kowitych na u�amek zwyk�y (pierwsza - licznik, druga - mianownik),
 *   - Liczby zmiennoprzecinkowej na u�amek zwyk�y. Dopuszczane jest obci�cie dok�adno�ci
 *     do kilku znak�w po przecinku,
 *   - Sta�ej tekstowej (const char *) na u�amek zwyk�y (np. "0.55" -> 55/100)
 *   - U�amka zwyk�ego na liczb� zmiennoprzecinkow�.
 *  Klasa powinna mie� metod� umo�liwiaj�c� dodanie u�amka zwyk�ego, oraz dwie wypisuj�ce
 *  licznik i mianownik (nominator, denominator).
 *
 *  Podpowied�: Pierwsze 4 konwersje nale�y zrobi� przy pomocy konstruktora, ostatni� - operatora.
 *  Podpowied�: Zamiana tekstu na liczb�: atoi(), atof(), atol()... (#include <cstdlib>)
 *
 *
 * Zadanie 2 (5.0): Doda� metod� upraszczaj�c� u�amek (zamiana np. 55/100 -> 11/20), oraz
 *  u�y� najmniejszej wsp�lnej wielokrotno�ci przy dodawaniu u�amk�w.
 *
 *  Podpowied�: Najwi�kszy wsp�lny dzielnik licznika i mianownika.
 *
 *
 * Zadanie b�dzie kontynuowane na nast�pnych zaj�ciach, dotycz�cych prze�adowania operator�w.
 *************************************************************************************************/

#include <cstdio>

#include <cmath>
#include <iostream>

#include "Fraction.h"
#define M_PI 3.14159265358979323846


double srednia(double a, double b ,double c)
{
    return (a+b+c)/3;
}
int main(int, const char **)
{
	Fraction a(4);
	Fraction b(3, 4);
	Fraction c(M_PI);

	printf("a: %u/%u: %f\n", a.nominator(), a.denominator(), (float)a);
	printf("b: %u/%u: %f\n", b.nominator(), b.denominator(), (float)b);
	printf("c: %u/%u: %f\n", c.nominator(), c.denominator(), (float)c);

	c.add(3);

	printf("c: %u/%u: %f\n", c.nominator(), c.denominator(), (float)c);

	b.add(Fraction(4, 5));
	printf("b: %u/%u: %f\n", b.nominator(), b.denominator(), (float)b);

	a=2.73;
	a.add("1.27");
	printf("a: %u/%u: %f\n", a.nominator(), a.denominator(), (float)a);

    printf("Srednia: %f\n", srednia(0.45f, b, Fraction(39, 39)));

    return 0;
}
