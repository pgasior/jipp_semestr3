#ifndef ARRAY_H
#define ARRAY_H

#include "Container.h"
#include "Fraction.h"
#include "Iterator.h"
#include <iostream>
#include <cstdlib>

using namespace std;

class unknown_element_exception : public std::exception
{

    virtual const char* what() const throw()
    {
        return "Probowano uzyskac dostep do nieistniejacego elementu ";
    }
} ;

template <class typ, int maxel> class Array;
template <class typ, int maxel> std::ostream& operator<< (std::ostream &out, Array<typ,maxel> &tablica);

template <class typ, int maxel>
class Array : public Container<typ>
{

public:
    class Iterator : public ::Iterator<typ>
    {
    public:
        Iterator(typ *fracptr) : fracptr(fracptr) {}
        Iterator(Array arr) {}
        Iterator& operator++()
        {
            fracptr++;
            return *this;
        }
        Iterator operator++(int)
        {
            Iterator tmp = fracptr;
            fracptr++;
            return tmp;
        }
        Iterator& operator--()
        {
            fracptr--;
            return *this;
        }
        Iterator operator--(int)
        {
            Iterator tmp = fracptr;
            fracptr--;
            return tmp;
        }
        bool operator==(const ::Iterator<typ>& arg2)
        {
            //cout << "Porownanie z Array\n";
            return fracptr==arg2.get();
        }
        bool operator!=(const ::Iterator<typ>& arg2)
        {
            return fracptr!=arg2.get();
        }
        bool operator<(const Iterator& arg2)
        {
            return fracptr<arg2.get();
        }
        bool operator>(const Iterator& arg2)
        {
            return fracptr>arg2.get();
        }
        Fraction* operator->()
        {
            return fracptr;
        }
        Fraction& operator*()
        {
            return *fracptr;
        }
        Fraction* get() const
        {
            return fracptr;
        }
        Iterator(const Iterator& it)
        {
            fracptr = it.get();
        }
    private:
        typ* fracptr;
        int indeks;
    };

    Array();
    Array(int size);
    ~Array();
    void push_back(typ newfrac);
    Iterator begin();
    Iterator end();
    int size();
    void clear();
    typ& operator[](int i);
    friend std::ostream& operator<< <>(std::ostream &out, Array<typ,maxel> &tablica);

protected:
private:
    typ* tab;
    int _size;
};

//template <int maxel> class Array<bool>;
template <int maxel> std::ostream& operator<< (std::ostream &out, Array<bool,maxel> &tablica);

template <int maxel>
class Array<bool, maxel>
{
public:
    Array();
    Array(int size);
    ~Array();
    void push_back(bool newfrac);
    int getSize();
    int getCapacity();
    bool operator[](int i);
    friend std::ostream& operator<< <>(std::ostream &out, Array<bool,maxel> &tablica);

protected:
private:
    unsigned char* tab;
    int _size;
    int elementy;
};

template<int maxel>
std::ostream& operator<< (std::ostream &out, Array<bool,maxel> &tablica)
{
    for(int i=0;i<tablica.elementy;i++)
    {
        out << tablica[i] << " ";
    }
    return out;
}

template<int maxel>
Array<bool,maxel>::Array()
{
    _size=0;
    elementy=0;
    try
    {
        tab = new unsigned char[0];
    }

    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge utowrzyc tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
}

template<int maxel>
Array<bool,maxel>::Array(int size)
{
    elementy=size;
    _size=(elementy-1/8)+1;
    if(elementy>maxel)
        throw too_big_exception();
    try
    {
        tab = new unsigned char[_size];
    }

    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge utowrzyc tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
}

template<int maxel>
Array<bool,maxel>::~Array()
{
    if(tab!=NULL)
        delete[](tab);
}

template<int maxel>
void Array<bool,maxel>::push_back(bool newfrac)
{
    if((elementy+1)>maxel)
        throw too_big_exception();

    if(((elementy)%8 ==0))
    {
        elementy++;
        _size=(elementy-1)/8+1;
        unsigned char *tab_new;
        try
        {
            tab_new = new unsigned char[_size];
        }
        catch (std::bad_alloc& ba)
        {
            std::cerr << "Nie moge dodac elementu do tablicy. bad_alloc: " << ba.what() << '\n';
            exit(1);
        }
        for(int i=0; i<_size-1; i++)
            tab_new[i]=tab[i];
        delete[](tab);
        tab=tab_new;
        tab[_size-1]=newfrac;
    }
    else
    {
        tab[_size-1] = tab[_size-1] | (newfrac<<elementy%8);
        elementy++;
    }
}

template<int maxel>
bool Array<bool,maxel>::operator[] (int i)
{
    if(i<0 || i>=elementy)
        throw unknown_element_exception();
    unsigned int el = (unsigned int)i;
    return tab[el/8] & (1<<(el%8));
}

template<int maxel>
int Array<bool,maxel>::getSize()
{
    return elementy;

}

template<int maxel>
int Array<bool,maxel>::getCapacity()
{
    return _size*8;
}


template <class typ, int maxel>
Array<typ,maxel>::Array()
{
    _size=0;
    try
    {
        tab = new typ[_size];
    }

    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge utowrzyc tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
}

template <class typ, int maxel>
std::ostream& operator<< (std::ostream &out, Array<typ,maxel> &tablica)
{
    for(int i=0;i<tablica.size();i++)
        out << tablica[i] << " ";
    return out;
}

template <class typ, int maxel>
Array<typ,maxel>::Array(int size)
{
    _size=size;
    if(size>maxel)
        throw too_big_exception();
    try
    {
        tab = new typ[_size];
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge utowrzyc tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
}

template <class typ, int maxel>
Array<typ,maxel>::~Array()
{
    if(tab!=NULL)
        delete[](tab);
}

template <class typ, int maxel>
void Array<typ,maxel>::push_back(typ newfrac)
{
    if((_size+1)>maxel)
        throw too_big_exception();
    _size++;
    typ *tab_new;
    try
    {
        tab_new = new typ[_size];
    }
    catch (std::bad_alloc& ba)
    {
        std::cerr << "Nie moge dodac elementu do tablicy. bad_alloc: " << ba.what() << '\n';
        exit(1);
    }
    for(int i=0; i<_size-1; i++)
        tab_new[i]=tab[i];
    delete[](tab);
    tab=tab_new;
    tab[_size-1]=newfrac;
}



template <class typ, int maxel>
int Array<typ,maxel>::size()
{
    return _size;
}
template <class typ, int maxel>
void Array<typ,maxel>::clear()
{
    delete(tab);
    tab=NULL;
}
template <class typ, int maxel>
typ& Array<typ,maxel>::operator[] (int i)
{
    if(i<0 || i>=_size)
        throw unknown_element_exception();
    return tab[i];
}
template <class typ, int maxel>
typename Array<typ,maxel>::Iterator Array<typ,maxel>::begin()
{
    return Array<typ,maxel>::Iterator(tab);
}
template <class typ, int maxel>
typename Array<typ,maxel>::Iterator Array<typ,maxel>::end()
{
    return Array<typ,maxel>::Iterator(tab+_size);
}




#endif // ARRAY_H
