#include <iostream>
#include "Array.h"
#include "MyList.h"
#include <exception>
using namespace std;

int main()
{
    MyList<int> lista;
    for(int i=10;i>0;i--)
        lista.push_back(i);

    Array<int,10> tablica;
    try
    {
        for(int i=0;i<15;i++)
            tablica.push_back(i);
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }

    Array<bool,10> bitfield;
    try
    {
        for(int i=0;i<17;i++)
            bitfield.push_back(i%2);
    }
    catch(exception& e)
    {
        cout << e.what() << endl;
    }

    cout << "Lista przed sortowaniem: " << lista << endl;
    lista.sortuj();
    cout << "Lista po sortowaniu:\t " << lista << endl;
    cout << "Tablica: " << tablica << endl;
    cout << "Bitfield: " << bitfield << endl;

    //cout << bitfield.getCapacity();
    cout << "Hello world!" << endl;
    return 0;
}
