#ifndef MYLIST_H
#define MYLIST_H

#include "Container.h"
#include "Fraction.h"
#include "Iterator.h"
class before_first_exception : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Probowano ustawic iterator przed pierwszy element listy";
    }
} ;
class after_last_exception : public std::exception
{
    virtual const char* what() const throw()
    {
        return "Probowano ustawic iterator za ostatni element listy";
    }
} ;

template <class typ> class MyList;
template <class typ> std::ostream& operator<< (std::ostream &out, MyList<typ> &lista);

template <class typ>
class MyList : public Container<typ>
{
public:
    class Node
    {
    public:
        Node();
        Node* getPrev();
        Node* getNext();
        typ* getVal();
        void setPrev(Node* newPrev);
        void setNext(Node* newNext);
        void setVal(typ* newVal);

    private:
        Node *next;
        Node *prev;
        typ *val;
    };
    class Iterator : public ::Iterator<typ>
    {
    public:
        Iterator(Node *fracnode) : fracnode(fracnode)
        {
            if(fracnode==NULL)
                fracptr=NULL;
            else fracptr=fracnode->getVal();
        }
        Iterator& operator++()
        {
            if((fracptr==NULL) || (fracnode->getNext()==NULL))
                throw after_last_exception();
            fracnode=fracnode->getNext();
            fracptr=(fracnode->getVal());
            return *this;
        }
        Iterator operator++(int)
        {
            //if((fracptr==NULL) || (fracnode->getNext()->getVal()==NULL))
            if((fracptr==NULL) || (fracnode->getNext()==NULL))
                throw after_last_exception();
            Iterator tmp = fracnode;
            fracnode=fracnode->getNext();
            fracptr=(fracnode->getVal());
            return tmp;
        }
        Iterator& operator--()
        {
            if(fracnode->getPrev()==NULL)
                throw before_first_exception();
            fracnode=fracnode->getPrev();
            fracptr=fracnode->getVal();
            return *this;
        }
        Iterator operator--(int)
        {
            if(fracnode->getPrev()==NULL)
                throw before_first_exception();
            Iterator tmp = fracnode;
            fracnode=fracnode->getPrev();
            fracptr=fracnode->getVal();
            return tmp;
        }
        bool operator==(const ::Iterator<typ>& arg2)
        {
            return fracptr==arg2.get();
        }
        bool operator!=(const ::Iterator<typ>& arg2)
        {
            return fracptr!=arg2.get();
        }
        typ* operator->()
        {
            return (fracnode->getVal());
        }
        typ& operator*()
        {
            return *fracnode->getVal();
        }
        Node* getnode() const
        {
            return fracnode;
        }
        typ* get() const
        {
            return fracptr;
        }
        Iterator(const Iterator& it)
        {
            fracnode = it.getnode();
        }


    private:
        Node* fracnode;
        typ *fracptr;
    };
    MyList();
    ~MyList();
    void push_back(typ frac);
    Iterator begin();
    Iterator end();
    int size();
    friend std::ostream& operator<< <>(std::ostream &out, MyList<typ> &lista);
    void sortuj();

protected:
private:
    Node *front;
    Node *back;
    Node *list_end;
    //Fraction *fracptr;
};


template <class typ>
std::ostream& operator<< (std::ostream &out, MyList<typ> &lista)
{
    typename MyList<typ>::Node *tmp = lista.front;
    while(tmp!=lista.list_end &&tmp!=NULL)
    {
        if (tmp->getVal());
            out << *(tmp->getVal()) << " ";
        tmp=tmp->getNext();
    }
    return out;
}

template<class typ>
void MyList<typ>::sortuj()
{
    int tmpsize=this->_size;
    //std::cout << tmpsize << std::endl;
    typename MyList<typ>::Node *tmp = front;
    do
    {
        for(int i=0;i<tmpsize-1;i++)
        {
            //std::cout << "i: " << *(tmp->getVal()) <<"\t\ti+1: "<<*(tmp->getNext()->getVal()) << std::endl;
            if(*(tmp->getVal())>*(tmp->getNext()->getVal()))
            {
                typ* tmpval = tmp->getVal();
                tmp->setVal(tmp->getNext()->getVal());
                tmp->getNext()->setVal(tmpval);
            }
            tmp=tmp->getNext();
        }
        tmpsize--;
        tmp=front;
    }while(tmpsize>1);


}
template <class typ>
MyList<typ>::MyList()
{
    list_end = new MyList<typ>::Node();
    //list_end->setVal(NULL);
    list_end->setNext(NULL);
    list_end->setNext(NULL);
    front = back = NULL;// fracptr =NULL;

    this->_size = 0;
}

template <class typ>
MyList<typ>::~MyList()
{
    while((front!=NULL) && front->getNext()!=NULL)
    {
        MyList<typ>::Node* tmp = front->getNext();
        delete front->getVal();
        delete front;
        front = tmp;
    }
    delete list_end;
}

template <class typ>
void MyList<typ>::push_back(typ frac)
{
    if((this->_size+1)>20)
        throw too_big_exception();
    MyList<typ>::Node *p = new MyList<typ>::Node();
    typ *tmpfrac = new typ(frac);
    p->setVal(tmpfrac);
    if(back) back->setNext(p);
    p->setNext(list_end);
    p->setPrev(back);
    back = p;
    list_end->setPrev(back);
    //fracptr = *(back->getVal());
    if(!front) front = back;
    this->_size++;
}

template <class typ>
typename MyList<typ>::Iterator MyList<typ>::begin()
{
    return Iterator(front);
}

template <class typ>
typename MyList<typ>::Iterator MyList<typ>::end()
{
    return Iterator(list_end);
}

template <class typ>
int MyList<typ>::size()
{
    return this->_size;
}

template <class typ>
MyList<typ>::Node::Node()
{
    prev = next = NULL;
    val = NULL;
}

template <class typ>
typename MyList<typ>::Node* MyList<typ>::Node::getPrev()
{
    return prev;
}

template <class typ>
typename MyList<typ>::Node* MyList<typ>::Node::getNext()
{
    return next;
}

template <class typ>
typ* MyList<typ>::Node::getVal()
{
    return val;
}

template <class typ>
void MyList<typ>::Node::setPrev(MyList<typ>::Node* newPrev)
{
    prev = newPrev;
}
template <class typ>
void MyList<typ>::Node::setNext(MyList<typ>::Node* newNext)
{
    next = newNext;
}

template <class typ>
void MyList<typ>::Node::setVal(typ* newVal)
{
    val = newVal;
}

#endif // MYLIST_H
