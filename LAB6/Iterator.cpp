#include "Iterator.h"
#include "Fraction.h"
Iterator::Iterator() {}
Iterator::Iterator(Fraction *frac)
{
    fracptr=frac;
}

void Iterator::operator=( Fraction *frac)
{
    fracptr = frac;
}
Fraction* Iterator::operator->()
{
    return fracptr;
}

Iterator Iterator::operator++(int) //postfix
{
    Iterator tmp = fracptr;
    fracptr++;
    return tmp;
}
Iterator& Iterator::operator++()    //prefix
{
    fracptr++;
    return *this;
}
Iterator Iterator::operator--(int) //postfix
{
    Iterator tmp = fracptr;
    fracptr--;
    return tmp;
}
Iterator& Iterator::operator--()     //prefix
{
    fracptr--;
    return *this;
}

Fraction& Iterator::operator*()
{
    return *fracptr;
}

