#include <cstdio>
#include <cmath>
#include <iostream>
#include "Fraction.h"
#include "Iterator.h"
#define M_PI 3.14159265358979323846

using std::cout;
using std::endl;

int main(int, const char **)
{
    Fraction u[5];
    u[0] =Fraction(3);
    u[1] =Fraction(4);
    u[2] = Fraction(5,6);
    u[3] = Fraction("4.2");
    u[4] = Fraction(2.46);
    Iterator it = u;
    for(int i=0;i<5;i++)
    {
        cout << (*it).nominator() <<","<<it->denominator() << endl;
        ++it;
    }
    Fraction u1(5,6);
    it=&u1;
    cout <<it->nominator() <<","<<it->denominator() << endl;
    return 0;
}
