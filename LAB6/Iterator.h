#ifndef ITERATOR_H_INCLUDED
#define ITERATOR_H_INCLUDED
#include "Fraction.h"
class Iterator
{
public:
    Iterator();
    Iterator(Fraction *frac);
    Fraction& operator*();
    void operator=(Fraction *frac);
    Fraction* operator->();
    Iterator operator++(int); //postfix
    Iterator& operator++();     //prefix
    Iterator operator--(int); //postfix
    Iterator& operator--();     //prefix


private:
    Fraction *fracptr;
};

#endif // ITERATOR_H_INCLUDED
